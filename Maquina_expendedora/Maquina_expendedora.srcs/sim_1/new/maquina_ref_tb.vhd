library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity maquina_ref_tb is
end maquina_ref_tb;

architecture test of maquina_ref_tb is
  -- Inputs
  signal CLR_N, CLK, B1, B2, B3, B4, B5 : std_logic;

  -- Outputs
  signal Error, Salida : std_logic;

  component maquina_ref is
    port (
      CLR_N : in  std_logic;  -- Asynchronous negated reset
      CLK   : in  std_logic;  -- Positive clock
      B1    : in  std_logic;  -- 10cent
      B2    : in  std_logic;  -- 20cent
      B3    : in  std_logic;  -- 50cent
      B4    : in  std_logic;  -- 1�
      B5    : in  std_logic;  -- salida del error
      --SW    : in  std_logic; -- switch para elegir refresco
      Error : out std_logic;  -- LED error exceder importe
      Salida: out std_logic   -- LED importe correcto
    );
  end component;

  constant CLK_PERIOD : time := 1 sec / 100_000_000;  -- Clock period
  
begin
   -- Unit Under Test
  uut: maquina_ref
    port map (
      CLR_N  => CLR_N,
      CLK    => CLK,
      B1     => B1,
      B2     => B2,
      B3     => B3,
      B4     => B4,
      B5     => B5,
      Error  => Error,
      Salida => Salida
    );

  clkgen: process
  begin
    CLK <= '0';
    wait for 0.25 * CLK_PERIOD;
    CLK <= '1';
    wait for 0.25 * CLK_PERIOD;
  end process;  
  
  CLR_N <= '0' after 0.25 * CLK_PERIOD,
           '1' after 0.75 * CLK_PERIOD;
             
  tester: process
  begin
    wait until CLR_N = '1';
    wait until CLK = '1';
    wait for 0.25 * CLK_PERIOD;
    B2 <= '1';
    wait for 0.5 * CLK_PERIOD;
    B2 <= '0';
    
    B3 <= '1';
    wait for 0.5 * CLK_PERIOD;
    B3 <= '0';
    
    B2 <= '1';
    wait for 0.5 * CLK_PERIOD;
    B2 <= '0';
    
    B1 <= '1';      
    wait for 0.5 * CLK_PERIOD;
    B1 <= '0';
    
    wait for 2.75 * CLK_PERIOD;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;
end architecture;