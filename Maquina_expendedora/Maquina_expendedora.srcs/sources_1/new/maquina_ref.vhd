library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity maquina_ref is
  Port (   
    CLR_N : in  std_logic;  -- Asynchronous negated reset
    CLK   : in  std_logic;  -- Positive clock
    B1    : in  std_logic;  -- 10cent
    B2    : in  std_logic;  -- 20cent
    B3    : in  std_logic;  -- 50cent
    B4    : in  std_logic;  -- 1�
    B5    : in  std_logic;  -- salida del error
    --SW    : in  std_logic; -- switch para elegir refresco
    Error : out  std_logic; -- LED error exceder importe
    Salida: out std_logic   -- LED importe exacto
  );
end entity;

architecture Behavioral of maquina_ref is
  type state_t is (S0_esp, S1_10, S2_20, S3_30, S4_40, S5_50, S6_60, S7_70, S8_80, S9_90, S10_sal, S11_error);
  signal state, nxt_state : state_t;
begin
  state_register: process (CLK, CLR_N)
  begin
    if CLR_N = '0' then
      state <= S0_esp;
    elsif rising_edge(CLK) then
      state <= nxt_state;
    end if;
  end process;
  
  nxtstate_decod: process(state, B1, B2, B3, B4, B5)
  begin
    nxt_state <= state;
    case state is
      when S0_esp =>
        if B1 = '1' then
          nxt_state <= S1_10;
        elsif B2 = '1' then
          nxt_state <= S2_20;
        elsif B3 = '1' then
          nxt_state <= S5_50;
        elsif B4 = '1' then
          nxt_state <= S10_sal;
        end if;
      when S1_10 =>
        if B1 = '1' then
          nxt_state <= S2_20;
        elsif B2 = '1' then
          nxt_state <= S3_30;
        elsif B3 = '1' then
          nxt_state <= S6_60;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S2_20 =>
        if B1 = '1' then
          nxt_state <= S3_30;
        elsif B2 = '1' then
          nxt_state <= S4_40;
        elsif B3 = '1' then
          nxt_state <= S7_70;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S3_30 =>
        if B1 = '1' then
          nxt_state <= S4_40;
        elsif B2 = '1' then
          nxt_state <= S5_50;
        elsif B3 = '1' then
          nxt_state <= S8_80;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S4_40 =>
        if B1 = '1' then
          nxt_state <= S5_50;
        elsif B2 = '1' then
          nxt_state <= S6_60;
        elsif B3 = '1' then
          nxt_state <= S9_90;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S5_50 =>
        if B1 = '1' then
          nxt_state <= S6_60;
        elsif B2 = '1' then
          nxt_state <= S7_70;
        elsif B3 = '1' then
          nxt_state <= S10_sal;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S6_60 =>
        if B1 = '1' then
          nxt_state <= S7_70;
        elsif B2 = '1' then
          nxt_state <= S8_80;
        elsif B3 = '1' then
          nxt_state <= S11_error;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S7_70 =>
        if B1 = '1' then
          nxt_state <= S8_80;
        elsif B2 = '1' then
          nxt_state <= S9_90;
        elsif B3 = '1' then
          nxt_state <= S11_error;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S8_80 =>
        if B1 = '1' then
          nxt_state <= S9_90;
        elsif B2 = '1' then
          nxt_state <= S10_sal;
        elsif B3 = '1' then
          nxt_state <= S11_error;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S9_90 =>
        if B1 = '1' then
          nxt_state <= S10_sal;
        elsif B2 = '1' then
          nxt_state <= S11_error;
        elsif B3 = '1' then
          nxt_state <= S11_error;
        elsif B4 = '1' then
          nxt_state <= S11_error;
        end if;
      when S10_sal =>
        if B5 = '1' then
          nxt_state <= S0_esp;
        end if;
      when S11_error =>
        if B5 = '1' then
          nxt_state <= S0_esp;
        end if;
      when others =>
        nxt_state <= S0_esp;
    end case;
  end process;
  
  output_decod: process(state)
  begin
  	case state is
      when S11_error =>
        Salida <= '0';
        Error  <= '1';
      when S10_sal =>
        Salida <= '1';
        Error  <= '0';
      when others =>
        Salida <= '0';
        Error  <= '0';
    end case;
  end process;
end architecture;